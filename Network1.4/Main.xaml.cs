﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Network1._4
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Page
    {
        private string getLocalIpAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
        String username;
        private DispatcherTimer timer = new DispatcherTimer(); //เวลา
        private Thread childSocketThread;
        public Main(String username)
        {
            InitializeComponent();
            this.username = username;
            listBoxChat.Items.Add("สวัสดี "+username);
            //listBoxPlayer.Items.Add(username);
            timer.Tick += receiveChat; //เลือกเมดตอด
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1); //จำนวนที่ให้รอ
            timer.Start();

        }

        public static void sendBroadcast(String a)
        {
            UdpClient client = new UdpClient();
            IPEndPoint ip = new IPEndPoint(IPAddress.Broadcast, 15000);
            byte[] bytes = Encoding.UTF8.GetBytes(a);
            client.Send(bytes, bytes.Length, ip);
            client.Close();
        }
        int sta = 1;
        public void receiveChat(object sender, object e)
        {
            if (sta == 1)
            {
                childSocketThread = new Thread(() =>
                {
                    //broadcast IP
                    UdpClient client = new UdpClient();
                    IPEndPoint ip = new IPEndPoint(IPAddress.Broadcast, 15000);
                    byte[] bytes = Encoding.UTF8.GetBytes(":" + username+"@"+ getLocalIpAddress());
                    client.Send(bytes, bytes.Length, ip);
                    client.Close();
                    //receive
                    Socket sock = new Socket(AddressFamily.InterNetwork,
                               SocketType.Dgram, ProtocolType.Udp);
                    IPEndPoint iep = new IPEndPoint(IPAddress.Any, 15000);
                    sock.Bind(iep);
                    EndPoint ep = (EndPoint)iep;
                    byte[] data = new byte[1024];
                    int recv = sock.ReceiveFrom(data, ref ep);
                    string message = Encoding.UTF8.GetString(data, 0, recv);
                    sock.Close();
                    
                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        
                         sta = 1;
                        if (message.StartsWith(":"))
                        {
                            message = message.Replace(":", null);
                            int count,sum=0;
                            int.TryParse(listBoxPlayer.Items.Count.ToString(),out count);
                            for (int i=0;i<count;i++) {
                                if (message==listBoxPlayer.Items.GetItemAt(i).ToString()) {
                                    sum++;
                                }
                            }
                            if (sum==0) {                              
                                listBoxPlayer.Items.Add(message);
                                //message = message.Split('@')[0].ToString();
                            }
                        }
                        else
                        {

                            listBoxChat.Items.Add(message);
                            // listBoxChat.Items.RemoveAt(1);
                        }
                    }));
                });
                childSocketThread.SetApartmentState(ApartmentState.STA);
                sta = 0;
                childSocketThread.Start();
            }
        }
        private void btnSend(object sender, RoutedEventArgs e)
        {
            listBoxPlayer.Items.Clear();
            String text = username+" : "+textBoxChat.Text;
            sendBroadcast(text);
            textBoxChat.Text = null;
            //this.NavigationService.Navigate(new Uri("Main.xaml",UriKind.RelativeOrAbsolute));
            //this.NavigationService.GoBack();

        }
        private void listBoxPlayer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(listBoxPlayer, e.OriginalSource as DependencyObject) as ListBoxItem;
            if (item != null)
            {
                String ip = item.ToString(); 
                ip = ip.Replace("System.Windows.Controls.ListBoxItem:",null);
                ip = ip.Split('@')[1].ToString();

            }
        }
    }
}
